### Apache2
/usr/sbin/service apache2 start

### MySQL
/bin/chown -R mysql:mysql /var/lib/mysql-datadir/
/usr/bin/mysql_install_db
/usr/sbin/service mysql start
/usr/bin/mysql -u root --execute="DELETE FROM mysql.user WHERE User='';";
/usr/bin/mysql -u root --execute="DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');";
/usr/bin/mysql -u root --execute="DROP DATABASE test;";
/usr/bin/mysql -u root --execute="DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';";
/usr/bin/mysqladmin -u root password 'secret'

### Docker ###
/bin/bash
