#
# Copyright (c) <2017> <Filippos A. Grapsas> public001@grapsas.com
# MIT License
#
# Crated at 14/01/2019
#

ARG BASE_IMAGE
FROM $BASE_IMAGE

ARG WWW_UID
ARG WWW_GID


### Main
ADD files/root/main.sh /root

### MariaDB
RUN mkdir /var/lib/mysql-datadir
ADD files/etc/mysql/my.cnf /etc/mysql/my.cnf

### Apache2
RUN sed -i "s/www-data:x:33:33/www-data:x:$WWW_UID:$WWW_GID/g" /etc/passwd
RUN mkdir -p /etc/php5/cli/conf.d/;\
    ln -s /etc/php5/mods-available/mcrypt.ini /etc/php5/cli/conf.d/20-mcrypt.ini;
RUN mkdir -p /etc/php5/apache2/conf.d/;\
    ln -s /etc/php5/mods-available/mcrypt.ini /etc/php5/apache2/conf.d/20-mcrypt.ini
ADD files/etc/apache2/conf-enabled/ubuntu-14.04-dev.conf /etc/apache2/conf-enabled
RUN mkdir -p /etc/apache2/vhosts
RUN mkdir -p /srv/www/vhosts; \
    chown -R www-data:www-data /srv/www/vhosts

RUN a2enmod rewrite expires proxy proxy_http headers

RUN mkdir -p /var/www/.composer/; \
    mkdir -p /var/www/.npm/; \
    mkdir -p /var/www/.config/; \
    mkdir -p /var/www/.cache/; \
    mkdir -p /var/www/.local/; \
    chown -R www-data:www-data /var/www/.composer/; \
    chown -R www-data:www-data /var/www/.npm/; \
    chown -R www-data:www-data /var/www/.config/; \
    chown -R www-data:www-data /var/www/.cache/; \
    chown -R www-data:www-data /var/www/.local/
